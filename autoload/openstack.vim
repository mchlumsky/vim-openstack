" Vim global plugin for OpenStack
" Maintainer: Martin Chlumsky <martin.chlumsky@gmail.com>

let s:currentCloud = ''
let s:currentResource = ''

fun openstack#switchCloud(name)
    let s:currentCloud = a:name
endfun

fun s:setupViewBuffer()
    python3 openstack.setup_view_buffer()
endfun

fun s:redrawViewBuffer() abort
    python3 openstack.redraw_view_buffer()
endfun

fun openstack#listResource(resource) abort
    if s:currentCloud ==# ''
        echo 'ERROR: Please set a cloud with OSsetCloud command.'
        return
    endif
    let s:currentResource = len(a:resource) ? a:resource : 'server'
    call s:setupViewBuffer()
    call s:redrawViewBuffer()
endfun

fun s:editResource(resourcename, bufType)
    python3 openstack.edit_resource(vim.eval('a:resourcename'), vim.eval('a:bufType'))
endfun

fun openstack#allClouds(A, L, P)
    return py3eval('openstack.get_all_clouds()')
endfunction

fun openstack#allResources(A, L, P)
    return py3eval('"\n".join(sorted(openstack.resources))')
endfun
