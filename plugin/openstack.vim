" Vim global plugin for OpenStack
" Maintainer: Martin Chlumsky <martin.chlumsky@gmail.com>

if exists('g:loaded_openstack')
    finish
endif
let g:loaded_openstack = 1

if !has('python3')
    echo 'VIM has to be compiled with +python3 to run VIM openstack'
    finish
endif

if !exists('g:os_show_headers')
    let g:os_show_headers = 1
endif

let s:has_pyyaml = 0
python3 << EOF
try:
    from yaml import load
except ImportError:
    pass
else:
    vim.command("let s:has_pyyaml=1")
EOF
if !s:has_pyyaml
    echo 'VIM openstack plugin requires pyyaml. Not loading.'
    finish
endif

let s:plugin_root_dir = fnamemodify(resolve(expand('<sfile>:p')), ':h')
python3 << EOF
import sys
from os.path import normpath, join
import vim
plugin_root_dir = vim.eval('s:plugin_root_dir')
python_root_dir = normpath(join(plugin_root_dir, '..', 'python'))
sys.path.insert(0, python_root_dir)
import openstack
EOF

command -bar -bang -complete=custom,openstack#allClouds -nargs=? OSsetCloud call openstack#switchCloud(<q-args>)
command -bar -bang -complete=custom,openstack#allResources -nargs=? OSlist call openstack#listResource(<q-args>)
