# Vim global plugin for OpenStack
# Maintainer: Martin Chlumsky <martin.chlumsky@gmail.com>

import json
import os
import re
from abc import ABC
from subprocess import getoutput

from yaml import load

try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader

import vim

KEYSTONE_LIST_RESOURCES = (
    "catalog",
    "ec2 credentials",
    "federation domain",
    "federation project",
    "implied role",
    "region",
    "service provider",
)
KEYSTONE_SELECTABLE_RESOURCES = (
    "access rule",
    "application credential",
    "credential",
    "consumer",
    "domain",
    "endpoint",
    "endpoint group",
    "group",
    "identity provider",
    "limit",
    "mapping",
    "policy",
    "project",
    "registered limit",
    "role",
    "service",
    "trust",
    "user",
)

NOVA_LIST_RESOURCES = ("compute service", "host", "keypair", "usage")
NOVA_SELECTABLE_RESOURCES = (
    "aggregate",
    "flavor",
    "hypervisor",
    "server",
    "server group",
)

NEUTRON_LIST_RESOURCES = ("network service provider",)
NEUTRON_SELECTABLE_RESOURCES = (
    "address group",
    "address scope",
    "floating ip",
    "network agent",
    "network flavor",
    "network flavor profile",
    "network",
    "network meter",
    "network meter rule",
    "network qos policy",
    "network qos rule type",
    "network rbac",
    "network segment",
    "network segment range",
    "port",
    "router",
    "security group",
    "security group rule",
    "subnet",
    "subnet pool",
)

GLANCE_SELECTABLE_RESOURCES = ("image",)

OCTAVIA_SELECTABLE_RESOURCES = ("loadbalancer",)

resources = {}


class ListResource(ABC):
    """
    Resource that can be listed.
    """

    list_command = "openstack --os-cloud {} {} list --format yaml"
    header = """\
#
# Press <tab> to move between resources
# Press gr to refresh
#
"""

    def __init__(
        self,
        resource_name,
        current_cloud=None,
    ):
        self.resource_name = resource_name

    @classmethod
    def create(cls, resource_name):
        global resources
        resources[resource_name] = cls(resource_name=resource_name)

    @classmethod
    def _set_list_keymappings(cls):
        vim.command("nnoremap <silent><buffer> <tab> / \\(id\\): <esc>:nohl<enter>")
        vim.command("nnoremap <silent><buffer> gr :call <SID>redrawViewBuffer()<CR>")

    def display_list(self):
        def _add_blank_lines(data):
            lines = data.splitlines()
            first = lines.pop(0)
            yield first
            for line in lines:
                if line.startswith("- "):
                    yield ""
                yield line

        self._set_list_keymappings()

        if vim.eval("g:os_show_headers") == "1":
            for line in self.header.splitlines():
                vim.current.buffer.append(line)
            vim.current.buffer.append(f"# Resource: {self.resource_name}")
            vim.current.buffer.append("")

        self._current_cloud = vim.eval("s:currentCloud")
        output = getoutput(
            self.list_command.format(self._current_cloud, self.resource_name)
        )
        vim.current.buffer.append(list(_add_blank_lines(output)))

        del vim.current.buffer[0]


class SelectableListResource(ListResource):
    """
    Resource that can be listed and selected
    """

    show_command = "openstack --os-cloud {} {} show {} --format json"
    header = """\
#
# Press <tab> to move between resources
# Press ii while on the resource ID line to get resource
# Press is while on the resource ID line to get resource in a split
# Press vs while on the resource ID line to get resource in a vertical split
# Press it while on the resource ID line to get resource in a new tab
# Press gr to refresh
#
"""

    def _set_list_keymappings(self):
        vim.command("nnoremap <silent><buffer> <tab> / \\(id\\): <esc>:nohl<enter>")
        vim.command(
            f"nnoremap <silent><buffer> ii :call <SID>editResource('{self.resource_name}', 'edit')<enter>"
        )
        vim.command(
            f"nnoremap <silent><buffer> is :call <SID>editResource('{self.resource_name}', 'sp')<enter>"
        )
        vim.command(
            f"nnoremap <silent><buffer> vs :call <SID>editResource('{self.resource_name}', 'vs')<enter>"
        )
        vim.command(
            f"nnoremap <silent><buffer> it :call <SID>editResource('{self.resource_name}', 'tabe')<enter>"
        )
        #  nnoremap <silent><buffer> dd :call <SID>deleteResource()<CR>
        vim.command("nnoremap <silent><buffer> gr :call <SID>redrawViewBuffer()<CR>")

    def display_show(self, buffer_type):
        match = re.search(" id: (?P<id>\\S+)", vim.current.line, flags=re.I)
        if match is None:
            return
        id_ = match.groupdict()["id"]

        output = getoutput(
            self.show_command.format(
                vim.eval("s:currentCloud"), self.resource_name, id_
            )
        )

        vim.command(f"{buffer_type} __{self.resource_name}_{id_}")
        vim.command("set modifiable")
        vim.command("set filetype=yaml")
        vim.current.buffer.options["buftype"] = "nofile"
        vim.current.buffer.options["bufhidden"] = "wipe"

        if vim.eval("g:os_show_headers") == "1":
            header = f"""\
# Resource: {self.resource_name}

"""
            for line in header.splitlines():
                vim.current.buffer.append(line)

        for key, value in json.loads(output).items():
            if "\n" in str(value):
                value = value.split("\n")
            vim.current.buffer.append(f"{key}: {value}")

        del vim.current.buffer[0]

        vim.command("set nomodified")


for list_resources in (
    KEYSTONE_LIST_RESOURCES,
    NOVA_LIST_RESOURCES,
    NEUTRON_LIST_RESOURCES,
):
    for resource in list_resources:
        ListResource.create(resource)

for selectable_resources in (
    KEYSTONE_SELECTABLE_RESOURCES,
    NOVA_SELECTABLE_RESOURCES,
    NEUTRON_SELECTABLE_RESOURCES,
    OCTAVIA_SELECTABLE_RESOURCES,
    GLANCE_SELECTABLE_RESOURCES,
):
    for resource in selectable_resources:
        SelectableListResource.create(resource)


def get_all_clouds():
    for clouds_filepath in (
        "clouds.yaml",
        os.path.expanduser("~/.config/openstack/clouds.yaml"),
        "/etc/openstack/clouds.yaml",
    ):
        if os.path.exists(clouds_filepath):
            with open(clouds_filepath) as clouds_file:
                clouds = load(clouds_file, Loader=Loader)
                cloud_names = clouds["clouds"].keys()
                return "\n".join(sorted(cloud_names))


def edit_resource(resource_name, buffer_type):
    resources[resource_name].display_show(buffer_type)


def setup_view_buffer():
    bufwinnr = vim.Function("bufwinnr")
    existing = bufwinnr("__OPENSTACK__")
    if existing == -1:
        vim.command("silent! split __OPENSTACK__")
        vim.current.buffer.options["buftype"] = "nofile"
        vim.current.buffer.options["bufhidden"] = "wipe"
        vim.command("set filetype=yaml")
    else:
        vim.command(f"silent! {existing}wincmd w")


def redraw_view_buffer():
    setup_view_buffer()
    vim.command("set modifiable")
    vim.command("%d")
    resources[vim.eval("s:currentResource")].display_list()
    vim.command("set nomodifiable")
